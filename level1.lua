-----------------------------------------------------------------------------------------
--
-- level1.lua
--
-----------------------------------------------------------------------------------------

local composer = require( "composer" )
local scene = composer.newScene()

-- include Corona's "physics" library
local physics = require "physics"

--------------------------------------------

-- forward declarations and other locals
local screenW, screenH, halfW, halfH = display.actualContentWidth, display.actualContentHeight, display.contentCenterX, display.contentCenterY
local enemysTable = {}
local topHeight, bottomHeight, points, lives = 50, 40, 5, 0, 5
local musicBacktracking
local playersTable = {}
local valueHeight = screenH/7
local died = false
local maxNumberOfCars, maxVelocity = 1, 20
local topSound = audio.loadSound( "top.wav" )
local endSound = audio.loadSound( "end.wav" )


math.randomseed( os.time() )

function scene:create( event )
	sceneGroup = self.view
	physics.start()
	physics.pause()
--	musicBacktracking = audio.loadStream( "1.wav")
	audio.reserveChannels( 1 )
	audio.setVolume( 0.1, { channel=1 } )

	local background = display.newImageRect( sceneGroup, "background.jpg", screenW, screenH )
	background.x = halfW
	background.y = halfH+30

	local top = display.newImageRect( sceneGroup, "end.jpg", screenW, topHeight )
	top.x = halfW
	top.y = 10
	top.myName = "top"
	physics.addBody( top, "static" )


	local bottom = display.newRect( sceneGroup, halfW, screenH-10, screenW, 1 )
	physics.addBody( bottom, "static" )

	resultText = display.newText( sceneGroup, "0", 5, 15, native.systemFont, 18 )
	restart()

end

function destructible()
	player.isBodyActive = true
end

function indestructible()
	player.isBodyActive = false
	timer.performWithDelay( 1000, destructible )
end

function createPlayer()
	player = display.newImage( "kite.png")
	player.x = halfW - 100
	player.y = screenH - 40
	player.myName = "player"
	physics.addBody( player, "dynamic" , { density=0.05, bounce=0.2})
	player.gravityScale = 0
	indestructible()
end

function createPlayers()
	createPlayer()
	for i = 1 , 2 do
		local player = display.newImage( sceneGroup, "kite.png")
		player.x = 25*i
		player.y = screenH-10
		table.insert(playersTable, player)
	end
end

function getVelocity()
	return math.random( 10, maxVelocity )
end

function getNumberOfCars()
	return math.random( 1, maxNumberOfCars )
end

function getPosition()
	local street = math.random( 1, 6 )
	local position = valueHeight + topHeight
	if street == 1 then
		return (position+10)/2
	elseif street == 2 then
		return position-7
	elseif street == 3 then
		return position + 38
	elseif street == 4 then
		return position + 85
	elseif street == 5 then
		return position + 130
	else
		return position + 174
	end
end

function getEnemy()
	local enemy = math.random( 1, 11 )
	if enemy == 1 then
		return "airplane.png"
	elseif enemy == 2 then
		return "drone.png"
	elseif enemy == 3 then
		return "airplane_3.png"
	elseif enemy == 4 then
		return "plane.png"
	elseif enemy == 5 then
		return "airplane_2.png"
	elseif enemy == 6 then
		return "space-capsule.png"
	elseif enemy == 7 then
		return "rocket.png"
	elseif enemy == 8 then
		return "spaceship.png"
	elseif enemy == 9 then
		return "spaceship_2.png"
	elseif enemy == 10 then
		return "satellite.png"
	elseif enemy == 11 then
		return "meteor.png"
	end
end

function createCar()

	local enemy = display.newImage(sceneGroup, getEnemy())
	enemy.height = 30
	enemy.width = 30
	enemy.x = display.viewableContentWidth + 100
	enemy.y = getPosition()

	physics.addBody( enemy, "dynamic" , { density=3.0, friction=0.5, bounce=0.3 })
	enemy.myName = "enemy"
	enemy:setLinearVelocity( -getVelocity(), 0 )
	table.insert( enemysTable, enemy )

	for i = #enemysTable, 1, -1 do
        local thisEnemy = enemysTable[i]

        if ( thisEnemy.x < -100 or
             thisEnemy.x > display.contentWidth + 100 or
             thisEnemy.y < -100 or
             thisEnemy.y > display.contentHeight + 100 )
        then
            display.remove( thisEnemy )
            table.remove( enemysTable, i )
        end
    end
end

local function restorePlayer()
		display.remove( player )
		died = false
		display.remove( playersTable[table.getn(playersTable)] )
		table.remove( playersTable, (table.getn(playersTable)) )
    createPlayer()

end

function restart()
	maxNumberOfCars, maxVelocity = 1, 20
	points = 0
	died = false
	createPlayers()
	physics.start()
end

function finishLevel()
	physics.pause()
	composer.setVariable( "finalScore", points )
  composer.gotoScene( "highscores", { time=800, effect="crossFade" } )
end

function increasedDifficult()
	maxVelocity = maxVelocity + 30
end

local function onCollision( event )

	local obj1 = event.object1
	local obj2 = event.object2


	if ( ( obj1.myName == "player" and obj2.myName == "enemy" ) or
			 ( obj1.myName == "enemy" and obj2.myName == "player" ) )
	then

			died = true
			player:removeSelf()
			for i = #enemysTable, 1, -1 do
					if ( enemysTable[i] == obj1 or enemysTable[i] == obj2 ) then
							table.remove( enemysTable, i )
							break
					end
			end
			if died then

				if table.getn(playersTable) > 0 then
					died = false
					audio.play( endSound )
					timer.performWithDelay( 2000, restorePlayer )
				else
					finishLevel()
				end
			end


		elseif obj1.myName == "player" and obj2.myName == "top" or
			obj2.myName == "player" and obj1.myName == "top" then
			display.remove( player )
			points = points + 100
			died = false
			audio.play( topSound )
			increasedDifficult()
			timer.performWithDelay( 2000, createPlayer )
	end

end

function createCars()
	local quantityOfCars = getNumberOfCars()
	for i = 1, quantityOfCars do
		createCar()
	end
end

local function gameLoop()

	if not died then
		points = points+1
	  resultText.text = points
	  createCars()
	end
end

function onTap(event)


if player.y ~= nil and event.y <= (screenH - 40) then
	if event.y > player.y then
		transition.moveBy( player, { x=0, y=45, time=200 } )
	else
		transition.moveBy( player, { x=0, y=-45, time=200 } )
	end
end

end

function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase

	if phase == "will" then
		-- Called when the scene is still off screen and is about to move on screen
	elseif phase == "did" then
		-- Called when the scene is now on screen
		--
		-- INSERT code here to make the scene come alive
		-- e.g. start timers, begin animation, play audio, etc.
		physics.start()
		physics.setGravity( -5.81, 0 )
		Runtime:addEventListener( "collision", onCollision )
    gameLoopTimer = timer.performWithDelay( 500, gameLoop, 0 )
	--	audio.play( musicBacktracking, { channel=1, loops=-1 } )
	end
end

function scene:hide( event )
	local sceneGroup = self.view

	local phase = event.phase

	if event.phase == "will" then
		timer.cancel( gameLoopTimer )
	elseif phase == "did" then
		Runtime:removeEventListener( "collision", onCollision )
        physics.pause()
        composer.removeScene( "level1" )
	end

end

function scene:destroy( event )
	local sceneGroup = self.view
	package.loaded[physics] = nil
	physics = nil
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

Runtime:addEventListener("tap", onTap)
Runtime:addEventListener( "postCollision", onCollision )


-----------------------------------------------------------------------------------------

return scene
