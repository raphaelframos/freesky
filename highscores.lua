local composer = require( "composer" )
local scene = composer.newScene()

local json = require( "json" )
local scoresTable = {}
filePath = system.pathForFile( "scores.json", system.DocumentsDirectory )


local function gotoMenu()
    composer.gotoScene( "menu", { time=800, effect="crossFade" } )
end

local function loadScores()

    local file = io.open( filePath, "r" )

    if file then
        local contents = file:read( "*a" )
        io.close( file )
        scoresTable = json.decode( contents )
    end

    if ( scoresTable == nil or #scoresTable == 0 ) then
        scoresTable = { 0, 0, 0 }
    end
end

local function saveScores()

    for i = #scoresTable, 4, -1 do
        table.remove( scoresTable, i )
    end

    local file = io.open( filePath, "w" )

    if file then
        file:write( json.encode( scoresTable ) )
        io.close( file )
    end
end

function scene:create( event )
	local sceneGroup = self.view

  loadScores()

  table.insert( scoresTable, composer.getVariable( "finalScore" ) )
  composer.setVariable( "finalScore", 0 )
  local function compare( a, b )
        return a > b
  end
  table.sort( scoresTable, compare )
  saveScores()



  local background = display.newImageRect( sceneGroup, "background.jpg", display.actualContentWidth, display.actualContentHeight )
  background.anchorX = 0
	background.anchorY = 0
	background.x = 0 + display.screenOriginX
	background.y = 0 + display.screenOriginY

  local highScoresHeader = display.newText( sceneGroup, "High Scores", display.contentCenterX, 100, native.systemFont, 44 )
  highScoresHeader:setFillColor( 0,0,1 )


    for i = 1, 3 do
        if ( scoresTable[i] ) then
            local yPos = 100 + ( i * 56 )

            local rankNum = display.newText( sceneGroup, i .. ")", display.contentCenterX-50, yPos, native.systemFont, 26 )
            rankNum:setFillColor( 0, 0, 1)
            rankNum.anchorX = 1

            local thisScore = display.newText( sceneGroup, scoresTable[i], display.contentCenterX-30, yPos, native.systemFont, 26 )
            thisScore.anchorX = 0
        end
    end
    local menuButton = display.newText( sceneGroup, "Menu", 10, 20, native.systemFont, 34 )
    menuButton:setFillColor( 1 ,1 , 1 )
    menuButton:addEventListener( "tap", gotoMenu )

end

function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase

	if phase == "will" then
		-- Called when the scene is still off screen and is about to move on screen
	elseif phase == "did" then
		-- Called when the scene is now on screen
		--
		-- INSERT code here to make the scene come alive
		-- e.g. start timers, begin animation, play audio, etc.
	end
end

function scene:hide( event )
	local sceneGroup = self.view
	local phase = event.phase

	if event.phase == "will" then
		-- Called when the scene is on screen and is about to move off screen
		--
		-- INSERT code here to pause the scene
		-- e.g. stop timers, stop animation, unload sounds, etc.)
	elseif phase == "did" then
    composer.removeScene( "highscores" )
	end
end

function scene:destroy( event )
	local sceneGroup = self.view

	-- Called prior to the removal of scene's "view" (sceneGroup)
	--
	-- INSERT code here to cleanup the scene
	-- e.g. remove display objects, remove touch listeners, save state, etc.

	if playBtn then
		playBtn:removeSelf()	-- widgets must be manually removed
		playBtn = nil
	end
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-----------------------------------------------------------------------------------------

return scene
